import javax.swing.*;
import java.awt.*;
import java.security.SecureRandom;
import java.util.Date;

/**
 * @author Jonathan Kula
 * @since 7:32 PM, 4/29/2016
 * in project JavaSecureRoll.
 * Copyright Jonathan Kula. All Rights Reserved.
 */
public class SecureRoll
{
    private JLabel  dp;
    private int     dpVal;
    private JLabel  d20;
    private int     d20Val;
    private JLabel  d12;
    private int     d12Val;
    private JLabel  d10;
    private int     d10Val;
    private JLabel  d8;
    private int     d8Val;
    private JLabel  d6;
    private int     d6Val;
    private JLabel  d4;
    private int     d4Val;
    private JLabel  d2;
    private int     d2Val;
    private JButton rollButton;
    private JPanel  panel;
    private JLabel  status;
    private JButton resetButton;
    private JButton addButton;
    private int absCount = 0;
    private int addCount = 0;
    private boolean adding;

    public SecureRoll()
    {
        SecureRandom sr = new SecureRandom();

        rollButton.addActionListener(e -> {
            int dpRand  = sr.nextInt(100) + 1;
            int d20Rand = sr.nextInt(20) + 1;
            int d12Rand = sr.nextInt(12) + 1;
            int d10Rand = sr.nextInt(10) + 1;
            int d8Rand  = sr.nextInt(8) + 1;
            int d6Rand  = sr.nextInt(6) + 1;
            int d4Rand  = sr.nextInt(4) + 1;
            int d2Rand  = sr.nextInt(2);

            if (!adding) {
                absCount++;
                dp.setText(String.valueOf(dpRand) + "%");
                d20.setText(String.valueOf(d20Rand));
                d12.setText(String.valueOf(d12Rand));
                d10.setText(String.valueOf(d10Rand));
                d8.setText(String.valueOf(d8Rand));
                d6.setText(String.valueOf(d6Rand));
                d4.setText(String.valueOf(d4Rand));
                d2.setText(d2Rand == 0 ? "H" : "T");
                status.setText("> Roll " + absCount + " @ " + new Date().toString());
            }
            else {
                addCount++;

                dpVal += dpRand;
                d20Val += d20Rand;
                d12Val += d12Rand;
                d10Val += d10Rand;
                d8Val += d8Rand;
                d6Val += d6Rand;
                d4Val += d4Rand;
                d2Val += d2Rand;

                dp.setText(String.valueOf(dpVal) + "%");
                d20.setText(String.valueOf(d20Val));
                d12.setText(String.valueOf(d12Val));
                d10.setText(String.valueOf(d10Val));
                d8.setText(String.valueOf(d8Val));
                d6.setText(String.valueOf(d6Val));
                d4.setText(String.valueOf(d4Val));
                d2.setText(String.valueOf(d2Val));

                status.setText("> Roll " + addCount + ". Adding...");
            }
        });

        addButton.addActionListener(e -> {
            adding = true;
            addButton.setEnabled(false);
            addButton.setText("Adding");
            rollButton.setText("Roll++");
            resetRolls();
            status.setText("> Roll 0. Adding...");
            status.setForeground(Color.GREEN.darker()
                                         .darker());
        });

        resetButton.addActionListener(e -> {
            addButton.setEnabled(true);
            rollButton.setText("Roll");
            addButton.setText("Add");
            if (adding) absCount++;
            adding = false;
            addButton.setEnabled(true);
            resetRolls();
            String plural = absCount == 1 ? " Roll." : " Rolls.";
            status.setText("> " + absCount + plural + " Reset.");
            status.setForeground(Color.BLACK);
        });
    }

    private void resetRolls()
    {
        dp.setText("__");
        d20.setText("__");
        d12.setText("__");
        d10.setText("__");
        d8.setText("__");
        d6.setText("__");
        d4.setText("__");
        d2.setText("__");

        dpVal = d20Val = d12Val = d10Val = d8Val = d6Val = d4Val = d2Val = 0;
    }

    public static void main(String[] args)
    {
        JFrame frame = new JFrame("SecureRoll");
        frame.setContentPane(new SecureRoll().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 700);
        frame.setVisible(true);
    }


}
